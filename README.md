# LazyDice	

A javascript project made in order to learn hoisting and different other awesome things like whatever the JS can do. 

## Getting Started

You need to have core javacript knowledge of some CSS HTML5

### Prerequisites

If you know, JS HTML5, and a little bit of deployment heheh you are good to go :P


### Installing

```
Just run some javascript code and have live reload if you likez it :D
```

I will add up a demo link here sooooooooon!!

## Running the tests

Test systems is still needed to be implemented...

## Deployment

Heroku and other deployment instructions shall soon be provided

## Built With

* JAVASCRIPT
* MongoDB
* HTML5
* CSS3
* HTML5
* some Coffee :D


## Contributing

Coming soon

## Authors

* **Himanshu Patel** - *Initial work* - [Thedesignable](https://github.com/thedesignable)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
